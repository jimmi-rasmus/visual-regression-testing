#!/bin/bash

echo "---------------"
echo "- Server watcher"
echo "---------------"

echo "Installing dependencies"
npm --prefix ./src/server install
echo "Starting server"
cd src/server/
./node_modules/nodemon/bin/nodemon.js --ignore 'static/*' index.js -e js,json,mustache
cd ../..