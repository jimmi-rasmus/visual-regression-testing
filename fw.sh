#!/bin/bash

echo "---------------"
echo "- Frontend watcher"
echo "---------------"

echo "Installing dependencies"
npm --prefix ./src/frontend install

echo "Building frontend"
npm --prefix ./src/frontend run start