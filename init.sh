#!/bin/bash
echo "Welcome to the visual regression testing test app"

echo "---------------"
echo "- Frontend"
echo "---------------"

echo "Installing dependencies"
npm --prefix ./src/frontend install

echo "Building frontend"
npm --prefix ./src/frontend run build

echo "---------------"
echo "- Server"
echo "---------------"

echo "Installing dependencies"
npm --prefix ./src/server install
echo "Starting server"
cd src/server/
node index.js
cd ../..
