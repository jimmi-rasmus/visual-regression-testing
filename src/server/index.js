const express = require('express');
const cookieparser = require('cookie-parser');
const mustacheExpress = require('mustache-express');

const app = express();

const port = 3000;
const host = '127.0.0.1';

// init cookieparser
app.use(cookieparser());

// Configure view engine
app.engine('mustache', mustacheExpress());
app.set('view engine', 'mustache')
app.set('views', __dirname + '/views');
app.set("layout", "index");

// Configure static files
app.use('/static', express.static('static'))

// Routes
app.get('/', (req, res) => res.render('home/index'));

app.get('/async', (req, res) => {
  res.render('async/index', { });
})

app.get('/async/*', (req, res) => {
  res.render('async/index', { });
})

app.get('/cookies', (req, res) => {
  const abTestValue = req.cookies['ab-test'];

  if(abTestValue === 'a'){
    res.render('cookies/a', { });
  }else{
    res.render('cookies/b', { });
  }
})

app.get('/interaction', (req, res) => {
  res.render('interaction/index', { });
})

app.get('/responsive', (req, res) => {
  res.render('responsive/index', { });
})

// routes for "api"

const products = [
  { id: 122, title: 'AEG Vaskemaskine l6e52', type: 'hvidevarer', description: 'lorem ipsum dolor' },
  { id: 123, title: 'AEG Vaskemaskine l6e54', type: 'hvidevarer', description: 'lorem ipsum dolor' },
  { id: 124, title: 'Bosch Vaskemaskine', type: 'hvidevarer', description: 'lorem ipsum dolor' },
  { id: 125, title: 'Siemens Opvaskemaskine', type: 'hvidevarer', description: 'lorem ipsum dolor' },
  { id: 225, title: 'MoccaMaster Kaffemaskine', type: 'koekkenudstyr', description: 'lorem ipsum dolor' },
  { id: 226, title: 'Sodastream Genesis sølv', type: 'koekkenudstyr', description: 'lorem ipsum dolor' },
  { id: 227, title: 'Wilfa vaffeljern', type: 'koekkenudstyr', description: 'lorem ipsum dolor' },
  { id: 327, title: 'LG OLED55C7', type: 'tv', description: 'lorem ipsum dolor' },
  { id: 328, title: 'Sony 49" UHD', type: 'tv', description: 'lorem ipsum dolor' },
  { id: 329, title: 'Samsung 55" 4K UHD', type: 'tv', description: 'lorem ipsum dolor' },
  { id: 439, title: 'Asus VS27 27" monitor', type: 'computer', description: 'lorem ipsum dolor' },
  { id: 440, title: 'TP-link deco mesh wifi', type: 'computer', description: 'lorem ipsum dolor' },
  { id: 441, title: 'Macbook pro 13"', type: 'computer', description: 'lorem ipsum dolor' },
  { id: 442, title: 'Asus VivoBook E403 14"', type: 'computer', description: 'lorem ipsum dolor' },
  { id: 443, title: 'HP Pavilion 15-bw04 15.6"', type: 'computer', description: 'lorem ipsum dolor' }
];

app.get('/api/products', (req, res) => {
  const type = req.query.type;
  let productsFiltered = products;

  if (type)
    productsFiltered = productsFiltered.filter(p => p.type.match(type));
  
  res.json(productsFiltered);
})

app.get('/api/products/:id', (req, res) => {
  const id = +req.params.id;
  product = products.find(p => p.id === id);
  res.json(product);
})

app.listen(port, () => console.log(`Server running at http://${host}:${port}!`))