import roadtrip from 'roadtrip';
import { 
    AllProductsView
    , HvidevareView
    , KoekkenView
    , ComputerView
    , TVView
    , ProductView
} from './views';

export function ProductsController(route, previousRoute){
    return route.data.then(function(data){
        route.view = AllProductsView({data, title: route._route.segments[1]});
    });
}

export function HvidevareController(route, previousRoute){
    return route.data.then(function(data){
        route.view = HvidevareView({data, title: route._route.segments[1]});
    });
}

export function KoekkenController(route, previousRoute){
    return route.data.then(function(data){
        route.view = KoekkenView({data, title: route._route.segments[1]});
    });
}

export function ComputerController(route, previousRoute){
    return route.data.then(function(data){
        route.view = ComputerView({data, title: route._route.segments[1]});
    });
}

export function TVController(route, previousRoute){
    return route.data.then(function(data){
        route.view = TVView({data, title: route._route.segments[1]});
    });
}

export function ProductController(route, previousRoute){
    return route.data.then(function(data){
        route.view = ProductView({data});
    });
}