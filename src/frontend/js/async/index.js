import roadtrip from 'roadtrip';
import { 
    ProductsController
    , ProductController
    , HvidevareController
    , KoekkenController
    , ComputerController
    , TVController
} from './controllers';
import API from './api';

(function(window){
    if(window.location.pathname.includes('/async')){
        initRouter();
        initLinkEvents();
    }
})(window);

function initRouter(){
    roadtrip
        .add('/async', { enter: ProductsController, beforeenter: API.Products })
        .add('/async/hvidevarer', { enter: HvidevareController, beforeenter: API.Products })
        .add('/async/koekkenudstyr', { enter: KoekkenController, beforeenter: API.Products })
        .add('/async/computer', { enter: ComputerController, beforeenter: API.Products })
        .add('/async/tv', { enter: TVController, beforeenter: API.Products })
        .add('/async/:id', { enter: ProductController, beforeenter: API.Product })
        .start({ fallback: '/async'});
}

function initLinkEvents(){
    document
        .querySelector('.async-navbar a')
        .addEventListener('click', function(e){
            e.preventDefault();
            roadtrip.goto(e.target.pathname)
        });
}