
export default {
    Products: Products
    , Product: Product
}

function Products(route){
    const type = route._route.segments[1] || '';
    route.data = fetch(`/api/products?type=${type}`)
        .then(response => response.json());
}

function Product(route){
    const id = route.params.id;
    route.data = fetch(`/api/products/${id}`)
        .then(response => response.json());
}