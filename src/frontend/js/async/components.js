import roadtrip from 'roadtrip';

const contentSelector = '.async-content';
const headerSelector = '.async-header';

export function ListProducts(products){
    const wrapper = document.querySelector(contentSelector);
    wrapper.innerHTML = '';

    const ul = document.createElement('ul');
    ul.id = 'products-list';
    ul.classList.add('products-list');

    products.forEach(p => {
        const li = document.createElement('li');

        const title = document.createElement('p');
        title.innerHTML = p.title;

        const img = document.createElement('img');
        img.src = `/static/images/${p.id}.jpg`;

        const btn = document.createElement('button');
        btn.innerHTML = `Læs mere`;
        btn.id = 'p' + p.id;
        btn.onclick = onListButtonClick.bind(null, p.id);

        li.appendChild(title);
        li.appendChild(img);
        li.appendChild(btn);
        ul.appendChild(li);
    });

    wrapper.appendChild(ul);
}

function onListButtonClick(id, e){
    roadtrip.goto(`/async/${id}`)
}

export function Header(title){
    document.querySelector(headerSelector).innerHTML = title;
}

export function ShowProduct(product){
    const wrapper = document.querySelector(contentSelector);
    wrapper.innerHTML = '';

    const fragment = document.createElement('div');
    fragment.id = "product";
    fragment.classList.add('product');

    const img = document.createElement('img');
    img.src = `/static/images/${product.id}.jpg`;

    const content = document.createElement('div');
    
    const p = document.createElement('p');
    p.innerHTML = product.description;

    const btn = document.createElement('button');
    btn.classList.add('button-primary');
    btn.innerHTML = 'Køb';
    btn.onclick = onProductBtnClick;

    fragment.appendChild(img);
    content.appendChild(p);
    content.appendChild(btn);
    fragment.appendChild(content);

    wrapper.appendChild(fragment); 
}

function onProductBtnClick(){
    alert('Så er den på vej til dig! 1-click ordering uden patent :)');
}
