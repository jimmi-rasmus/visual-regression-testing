import roadtrip from 'roadtrip';
import { 
    ListProducts
    , Header
    , ShowProduct
} from './components';

export function AllProductsView(props){
    Header('produkter');
    ListProducts(props.data);
}

export function HvidevareView(props){
    Header(props.title);
    ListProducts(props.data);
}

export function KoekkenView(props){
    Header(props.title);
    ListProducts(props.data);
}

export function ComputerView(props){
    Header(props.title);
    ListProducts(props.data);
}

export function TVView(props){
    Header(props.title);
    ListProducts(props.data);
}

export function ProductView(props){
    Header(props.data.title);
    ShowProduct(props.data);
}