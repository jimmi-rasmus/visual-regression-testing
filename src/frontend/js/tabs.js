(function(window){
    let tablinks, tab, tabId;

    tablinks = document
        .querySelectorAll('.tablinks');

    for (var i = 0; i < tablinks.length; i++) {
        tab = tablinks[i];
        tabId = tab.getAttribute("data-tab-id");
        tab.addEventListener("click", openTab.bind(this, tabId));
    }
})(window);

function openTab(tabId, event) {
    let tabcontents, tabcontent, tablinks, tablink;

    // Hide all active classes from tab content 
    tabcontents = document.querySelectorAll(".tabcontent");
    for (var i = 0; i < tabcontents.length; i++) {
        tabcontent = tabcontents[i];
        tabcontent.className = tabcontent.className.replace(" active", "");
    }

    // Remove all active classes from tab links
    tablinks = document.querySelectorAll(".tablinks");
    for (var i = 0; i < tablinks.length; i++) {
        tablink = tablinks[i];
        tablink.className = tablink.className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabId).className += " active";
    event.currentTarget.className += " active";
}
  