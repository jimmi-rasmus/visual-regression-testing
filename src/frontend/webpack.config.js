const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin'); 
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: 'production',
    entry: './main.js',
    output: {
        path: path.resolve(__dirname, '../server/static'),
        filename: '[name]-[chunkhash].js'
    },
    module: {
        rules: [{
            test: /\.(css|sass|scss)$/,
            use: [
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: {
                        importLoaders: 2,
                        sourceMap: true
                    }
                },
                {
                    loader: 'postcss-loader',
                    options: {
                        plugins: () => [
                            require('autoprefixer')
                        ],
                        sourceMap: true
                    }
                },
                {
                    loader: 'sass-loader',
                    options: {
                        sourceMap: true
                    }
                }
            ]
        }]
    },
    devtool: 'sourcemaps',
    plugins: [
        new CleanWebpackPlugin([path.resolve(__dirname, '../server/static')], { allowExternal: true }),
        new MiniCssExtractPlugin({
            filename: "[name]-[chunkhash].css",
        }),
        new HtmlWebpackPlugin({
            inject: true,
            template: 'index.mustache', //relative to root of the application
            filename: "../views/index.mustache"
        }),
        new CopyWebpackPlugin(['images/*'])
    ]
};